<?php

namespace App\Repository;

use App\Entity\Usereleve;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Usereleve|null find($id, $lockMode = null, $lockVersion = null)
 * @method Usereleve|null findOneBy(array $criteria, array $orderBy = null)
 * @method Usereleve[]    findAll()
 * @method Usereleve[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsereleveRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Usereleve::class);
    }

//    /**
//     * @return Usereleve[] Returns an array of Usereleve objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Usereleve
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
