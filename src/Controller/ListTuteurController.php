<?php

namespace App\Controller;

use App\Entity\Tuteur;
use App\Entity\Entreprise;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Form\FormFactoryInterface;
use App\Form\TuteurType;


class ListTuteurController extends Controller
{
    /**
     * @Route("/listTuteur", name="listTuteur")
     */
    public function index()
    {
        $tuteurs = $this->getDoctrine()
            ->getRepository(Tuteur::class)
            ->findAll();
        return $this->render('listTuteur/listTuteur.html.twig', compact('tuteurs'));
    }

    /**
     * @Route("/listTuteur/ajout", name="ajoutTuteur")
     */
    public function ajoutTuteur(Request $request, FormFactoryInterface $formFactory)
    {
        $form = $formFactory->createBuilder(TuteurType::class)->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
        }

        return $this->render('listTuteur/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/listTuteur/supprimer/{id}", name="supprimerTuteur")
     */
    public function supprimerTuteur($id)
    {
        $item = $this->getDoctrine()
            ->getRepository(Tuteur::class)
            ->find($id);

        if (!$item) {
            throw $this->createNotFoundException(
                "Aucun tuteur n'a été trouvée via l'id " . $id
            );
        }else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($item);
            $em->flush();
        }

        // Par défaut on retourne à la liste
        return $this->redirectToRoute('listTuteur');
    }

    /**
     * @Route("/listTuteur/modifier/{id}", name="modifierTuteur")
     */
    public function modifierTuteur(Request $request, $id)
    {
        $item = $this->getDoctrine()
            ->getRepository(Tuteur::class)
            ->find($id);
        if (!$item) {
            throw $this->createNotFoundException(
                "Aucun tuteur n'a été trouvée via l'id " . $id
            );
        } else {
            $form = $this->createFormBuilder($item)
                ->add('nomTuteur', TextType::class)
                ->add('prenomTuteur', TextType::class)
                ->add('mailTuteur', EmailType::class)
                ->add('telTuteur', TextType::class)
                ->add('entreprise', TextType::class)
                ->getForm();
        }
        // Par défaut, demande POST au même contrôleur qui la restitue.
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $item = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($item);
                $em->flush();
                return $this->redirectToRoute('listTuteur');
            }
        }
        return $this->render('listTuteur/form.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
