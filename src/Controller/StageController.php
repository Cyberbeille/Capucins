<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StageController extends Controller
{
    /**
     * @Route("/stage", name="stage")
     */
    public function index()
    {
        return $this->render('stage/stage.html.twig', [
            'controller_name' => 'StageController',
        ]);
    }
}
