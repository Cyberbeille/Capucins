<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AideController extends Controller
{
    /**
     * @Route("/aide", name="aide")
     */
    public function index()
    {
        return $this->render('aide/aide.html.twig', [
            'controller_name' => 'AideController',
        ]);
    }
}
