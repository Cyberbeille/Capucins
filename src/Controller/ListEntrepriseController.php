<?php

namespace App\Controller;

use App\Entity\Entreprise;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ListEntrepriseController extends Controller
{
    /**
     * @Route("/listEntreprise", name="listEntreprise")
     */
    public function index()
    {
        $entreprise = $this->getDoctrine()
            ->getRepository(Entreprise::class)
            ->findAll();
        return $this->render('listEntreprise/listEntreprise.html.twig', compact('entreprise'));
    }
}
